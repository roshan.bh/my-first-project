package com.company;
import java.util.Date;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
	    Date date = new Date();  // class = Date and object date.
       System.out.println("Current date and time is: \n" + date.toString()); // prints the current time and date.
	    int bornYear;
        System.out.println("Enter your born year: ");
        bornYear = input.nextInt();
        int currentYear = date.getYear() + 1900; // add 1900 to show the current year.
        int yourYear = currentYear - bornYear;
        System.out.println("You are " + yourYear + " Years old.");
    }
}
